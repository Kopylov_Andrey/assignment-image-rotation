#pragma once
#include "../include/image.h"
#include  <stdint.h>
#include <stdio.h>

#ifndef BMP_READER_H
#define BMP_READER_H

#pragma pack(push, 1)//выравнивание в 1 байт
struct bmp_header
{
    uint16_t bfType;                //Метка BM                      (2 байта)
    uint32_t bfileSize;             //Длина в байтах                (4 байта)
    uint32_t bfReserved;            //Резерв                        (4 байта)
    uint32_t bOffBits;              //Смещение области данных       (4 байта) 
    //BITMAP_INFO
    uint32_t biSize;                //Длина BITMAP_INFO             (4 байта)
    uint32_t biWidth;               //Ширина картинки               (4 байта)
    uint32_t biHeight;              //Высота картинки               (4 байта)
    uint16_t biPlanes;              //Число цветовый плоскостей     (2 байта)
    uint16_t biBitCount;            //Бит на пиксел                 (2 байта)
    uint32_t biCompression;         //Тип сжатия                    (4 байта)
    uint32_t biSizeImage;           //Размер изображения в байтах   (4 байта)
    uint32_t biXPelsPerMeter;       //Разрешение по горизонтали     (4 байта)
    uint32_t biYPelsPerMeter;       //Разрешение по вертикали       (4 байта)
    uint32_t biClrUsed;             //Количество используемых цветов(4 байта)
    uint32_t biClrImportant;        //Количество основных цветов    (4 байта)
};
#pragma pack(pop)

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_NOT_ALLOCATED_MEMORY
};

enum  write_status {
    WRITE_OK = 0,
    WRITE_ERROR_HEADER,
    WRITE_ERROR_DATA,
    WRITE_INVALID_SIGNATURE
};

enum read_status from_bmp(const char* input_file, struct bmp_header* bmp_header, struct image* img);
enum write_status to_bmp(const char* output_file,const struct bmp_header* bmp_file,const struct image* img);
void freeBMPFile(struct bmp_header* bmp_file);

#endif // !BMP_READER_H
