#pragma once
#include  <stdint.h>

#ifndef IMAGE_H
#define IMAGE_H

struct image
{
	uint32_t width;
	uint32_t height;
	unsigned char* pixel_data;
};

void freeImage(struct image* image);

uint32_t padding_calc(uint32_t widht);

uint32_t data_size_calc(uint32_t widht, uint32_t height, uint32_t padding, uint32_t bit_count);

#endif // IMAGE
