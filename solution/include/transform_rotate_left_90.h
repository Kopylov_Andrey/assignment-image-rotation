#pragma once
#include "../include/image.h"

#ifndef TRANSFOM_ROTATE_LEFT_90_H
#define TRANSFOM_ROTATE_LEFT_90_H

void rotate_left_90(const struct image* img_input, struct image* img_output);

#endif //!TRANSFOM_ROTATE_LEFT_90_H
