#include "../include/bmp_reader.h"
#include "../include/image.h"
#include <stdio.h>
#include <stdlib.h>

#ifndef BMP_READER_C
#define BMP_READER_C

enum read_status from_bmp(const char* input_file, struct bmp_header* bmp_header, struct image* img) {
    FILE* file_stream = fopen(input_file, "rb");
    if (!file_stream) return READ_INVALID_SIGNATURE;
   
    struct bmp_header* bmp_file = (struct bmp_header*)malloc(sizeof(struct bmp_header));
    if (!bmp_file) return READ_NOT_ALLOCATED_MEMORY;
   
    fread(bmp_file, sizeof(struct bmp_header), 1, file_stream);
    if (ferror(file_stream)) return READ_INVALID_HEADER;
    
    *bmp_header = *bmp_file;

    img->pixel_data = (unsigned char*)malloc(data_size_calc(bmp_file->biWidth, bmp_file->biHeight, 0, bmp_file->biBitCount/8));
    if (!img->pixel_data) return READ_NOT_ALLOCATED_MEMORY;
   
    fseek(file_stream, (long int)bmp_file->bOffBits, SEEK_SET);

    img->width = bmp_file->biWidth;
    img->height = bmp_file->biHeight;

    uint32_t padding = padding_calc(img->width);
   
    for (size_t i = 0; i < img->height; i++)
    {
        fread(&(img->pixel_data[i * img->width * 3]), img->width * 3, 1, file_stream);
        fseek(file_stream, (long int)padding, SEEK_CUR);
    }
    fclose(file_stream);    
    free(bmp_file);
    return READ_OK;
}

enum write_status to_bmp(const char* output_file,const struct bmp_header* bmp_file,const struct image* img) {
    const unsigned char empty_byte = 0x00;

    FILE* file_stream = fopen(output_file, "wb");
    if (!file_stream) return WRITE_INVALID_SIGNATURE;
  
    fwrite(bmp_file, sizeof(struct bmp_header), 1, file_stream);
    if (ferror(file_stream)) return WRITE_ERROR_HEADER;
   
    uint32_t padding = padding_calc(img->width);
    
    for (size_t i = 0; i < img->height; i++)
    {
        fwrite(&(img->pixel_data[i * img->width * 3]), img->width * 3, 1, file_stream);
        if(ferror(file_stream))return WRITE_ERROR_DATA;
        for (size_t j = 0; j < padding; j++)
            fwrite(&empty_byte, sizeof(unsigned char), 1, file_stream);
    }
    fclose(file_stream);
    return WRITE_OK;
}

void freeBMPFile(struct bmp_header* bmp_file) {
    if (bmp_file)
        free(bmp_file);
}
#endif 
