
#include "../include/image.h"
#include <stdlib.h>

#ifndef IMAGE_C
#define IMAGE_C

void freeImage(struct image* image) {
	if (image)
		free(image);
}

uint32_t padding_calc(uint32_t widht) {
	uint32_t padding = (4 - ((widht * 3) % 4)) % 4;
	return padding;
}

uint32_t data_size_calc(uint32_t widht, uint32_t height, uint32_t padding, uint32_t bit_count) {
	uint32_t size = widht * height * bit_count + padding * widht;//для 24 битных и для 32 битных файлов
	return size;
}

#endif 
