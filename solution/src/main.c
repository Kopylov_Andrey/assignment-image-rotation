#include "../include/bmp_reader.h"
#include "../include/image.h"
#include "../include/transform_rotate_left_90.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>



int main(int argc, char** argv) {
    
    struct bmp_header bmp_file;
    struct image img_input;
    struct image img_output;

    if (argc < 3) {
        printf("Argument that passes the bmp file is missing\n");
        return 0;
    }

    const char* input_file = argv[1];
    const char* output_file = argv[2];
    enum read_status read;
    enum write_status write;
   

    read = from_bmp(input_file, &bmp_file, &img_input);
    
    switch (read)
    {
    case READ_OK:
        printf("The reading was successful\n");
        break;  
    case READ_INVALID_SIGNATURE:
        printf("Could not open the file\n");
        break;
    case READ_INVALID_BITS:
        printf("The data was read with an error\n");
        break;
    case READ_INVALID_HEADER:
        printf("The title was read with an error\n");
        break;
    case READ_NOT_ALLOCATED_MEMORY:
        printf("No memory has been allocated\n");
        break;
    }

    rotate_left_90(&img_input, &img_output);

    const uint32_t padding = padding_calc(bmp_file.biHeight);
    const uint32_t biSizeImage = img_output.width * img_output.height * 3 + img_output.height *  padding;
    const uint32_t bfileSize = bmp_file.bOffBits + biSizeImage;

    bmp_file.biHeight = img_output.height;
    bmp_file.biWidth = img_output.width;
    bmp_file.biSizeImage = biSizeImage;
    bmp_file.bfileSize = bfileSize;
   
    write = to_bmp(output_file, &bmp_file, &img_output);
  
    switch (write)
    {
        case WRITE_OK:
          printf("the recording was successful\n");
          break;
        case WRITE_ERROR_HEADER:
            printf("Recording error header\n");
            break;
        case WRITE_ERROR_DATA:
            printf("Recording error data\n");
            break;
        case WRITE_INVALID_SIGNATURE:
            printf("Could not open the file\n");
            break;
    }

    free(img_input.pixel_data);
    free(img_output.pixel_data);
    
    return 0;
}
