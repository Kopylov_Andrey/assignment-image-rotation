#include "../include/transform_rotate_left_90.h"
#include "../include/image.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef TRANSFOM_ROTATE_LEFT_90_C
#define TRANSFOM_ROTATE_LEFT_90_C

void rotate_left_90(const struct image* img_input, struct image* img_output) {

	const uint32_t pixel_size = 3;
	const uint32_t transform_padding = padding_calc(img_input->height);

	struct image* img = (struct image*)malloc(sizeof(struct image));
	if (!img) exit(0);
	img->pixel_data = (unsigned char*)malloc(data_size_calc(img_input->width, img_input->height, transform_padding, pixel_size));
	if (!img->pixel_data) exit(0);
	img->height = img_input->width;
	img->width = img_input->height;

	size_t file_size = img->height * img->width * pixel_size;
	size_t row_size = img_input->width * pixel_size;

	for (size_t i = 0; i < file_size; i += row_size)
		for (size_t j = 0; j < row_size; j += pixel_size)
			for (size_t f = 0; f < pixel_size; f++)
				img->pixel_data[(img_input->height * (j + pixel_size) - (pixel_size - f) - i * pixel_size / row_size)] = img_input->pixel_data[i + j + f];

	*img_output = *img;
	free(img);
}
#endif
